# [maxima](https://tracker.debian.org/pkg/maxima)

Computer algebra system http://maxima.sourceforge.net/

* https://en.wikipedia.org/wiki/Maxima_(software)
* [DebianScienceMaxima](https://wiki.debian.org/DebianScienceMaxima)
* [DebianScienceComputerAlgebraSystems](https://wiki.debian.org/DebianScience/ComputerAlgebraSystems)

![](https://qa.debian.org/cgi-bin/popcon-png?packages=python3-sympy%20python-sympy%20maxima%20axiom%20yacas%20open-axiom&show_installed=on&want_legend=on&want_ticks=on&date_fmt=%25Y-%25m&beenhere=1)